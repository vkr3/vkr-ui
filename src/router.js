import Vue from 'vue'
import Router from 'vue-router'
import AgronomPage from "./components/AgronomPage";
import HomePage from "./components/HomePage";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomePage
        },
        {
            path: '/agronom',
            component: AgronomPage
        }
    ]
})