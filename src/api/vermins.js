import client from "./client/client";

export function getVermins() {
    return client.get('http://localhost:8090/vermin')
}

export function createVermin(vermin) {
    return client.post('http://localhost:8090/vermin', vermin)
}