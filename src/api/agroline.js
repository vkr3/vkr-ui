import client from "./client/client";

export function createProduct(product) {
    return client.post('http://localhost:8090/product',product);
}

export function getAllProduct() {
    return client.get('http://localhost:8090/product');
}

export function search(type) {
    return client.get('http://localhost:8090/product/search', {params: {type: type}});
}

export function deleteProduct(id) {
    return client.delete(`http://localhost:8090/product/${id}`);
}

export function getAllSupplier() {
    return client.get('http://localhost:8090/supplier');
}