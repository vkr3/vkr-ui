import client from "./client/client";

export function login(user) {
    return client.post('http://localhost:8090/user/login', user)
}

export function register(user) {
    return client.post('http://localhost:8090/user/register', user)
}