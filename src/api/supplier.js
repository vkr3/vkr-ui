import client from "./client/client";

export function createSupplier(supplier) {
    return client.post('http://localhost:8090/supplier', supplier);
}

export function updateSupplier(id, supplier) {
    return client.put(`http://localhost:8090/supplier/${id}`, supplier)
}